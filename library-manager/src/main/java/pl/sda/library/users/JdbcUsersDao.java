package pl.sda.library.users;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.sda.library.core.ConnectionFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class JdbcUsersDao implements IUsersDao {


    private static Logger logger = LoggerFactory.getLogger(JdbcUsersDao.class);
    private final ConnectionFactory connectionFactory;

    public JdbcUsersDao(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    public User findUser(String login, String password) throws SQLException {
        String sql = "SELECT id, login, password, name, admin " +
                "WHERE login =? AND password=?";
        try (Connection connection = connectionFactory.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            statement.setString(2, password);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                int id = resultSet.getInt("id");
                login = resultSet.getString("login");
                password = resultSet.getString("password");
                String name = resultSet.getString("name");
                boolean isAdmin = resultSet.getBoolean("admin");
                return new User(id, login, password, name, isAdmin);
            }
        }
        return null;
    }

    @Override
    public List<User> list(UserParameters userParameters) throws SQLException {
        List<User> users = new ArrayList<>();
        String sql = "SELECT id, login, password, name, is_admin FROM users";
        try (Connection connection = connectionFactory.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {

//przygotowanie listy filtrów do dodania do klauzuli WHERE

            List<String > filters = new ArrayList<>();
            if (userParameters.hasId()){
                filters.add("id =?");
            }
            if (userParameters.hasLogin()){
                filters.add("login = ?");
            }
            if (userParameters.hasPassword()){
                filters.add("password = ?");
            }
            if (!filters.isEmpty()){
                sql = "WHERE" + filters.stream().collect(Collectors.joining("AND"));
            }

            logger.info("SQL" + sql);

            //zmienna i pozwala nam dodać parametry w odpowiedniej kolejności

            int i = 1;
            if (userParameters.hasId()){
                statement.setInt(i++, userParameters.getId());
            }
            if (userParameters.hasLogin()){
                statement.setString(i++, userParameters.getLogin());
            }
            if (userParameters.hasPassword()){
                statement.setString(i, userParameters.getPassword());
            }

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                int id = resultSet.getInt("id");
                String login = resultSet.getString("loggin");
                String password = resultSet.getString("passowrd");
                String name = resultSet.getString("name");
                boolean isAdmin = resultSet.getBoolean("is_admin");

                users.add(new User(id, login, password, name, isAdmin));
            }
        }
        return users;
    }

    @Override
    public void addUser(User user) throws SQLException {

        String  sql = "INSERT INTO users(login, password, name, is_Admin) VALUE (?, ?, ?, ?);";
        try (Connection connection = connectionFactory.getConnection();
        PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getName());
            statement.setBoolean(4, user.isAdmin());

            statement.executeUpdate();
        }
    }

    @Override
    public void deleteUser(int userId) throws SQLException {
        String  sql = "DROP users WHERE id=?";
        try (Connection connection = connectionFactory.getConnection();
        PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setInt(1, userId);

            statement.executeUpdate();
            logger.info("User delete, id{}", userId);

        }
    }
}