package pl.sda.library.users;

import java.sql.SQLException;
import java.util.List;

public interface IUsersDao {
    User findUser(String login, String password) throws SQLException;
    List<User> list(UserParameters userParameters) throws SQLException;

    void addUser(User user) throws SQLException;
    void deleteUser(int userId) throws SQLException;
}
