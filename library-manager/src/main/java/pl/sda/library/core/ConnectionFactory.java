package pl.sda.library.core;

import com.mysql.cj.jdbc.MysqlDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionFactory {
    private static Logger logger = LoggerFactory.getLogger(ConnectionFactory.class);
    private static DataSource dataSource;

    public ConnectionFactory() {
        // Krok 2
        this("/database.properties");
    }
    public ConnectionFactory(String filename) {
        // Krok 3
        Properties properties = getDataBaseProperties(filename);
        try {
            // Krok 5
            dataSource = getDataSource(properties);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    private Properties getDataBaseProperties(String filename) {
        // Krok 4
        Properties properties = new Properties();
        try {
            /**
             * Pobieramy zawartość pliku za pomocą classloadera, plik musi znajdować się w katalogu ustawionym w CLASSPATH
             */
            InputStream propertiesStream = ConnectionFactory.class.getResourceAsStream(filename);
            if(propertiesStream == null) {
                throw new IllegalArgumentException("Can't find file: " + filename);
            }
            /**
             * Pobieramy dane z pliku i umieszczamy w obiekcie klasy Properties
             */
            // Krok 5
            properties.load(propertiesStream);
        } catch (IOException e) {
            logger.error("Error during fetching properties for database", e);
            return null;
        }
        return properties;
    }
    private DataSource getDataSource(Properties properties) throws SQLException {
        // Krok 6
        String serverName = properties.getProperty("pl.sda.jdbc.db.server");
        String databaseName = properties.getProperty("pl.sda.jdbc.db.name");
        String user = properties.getProperty("pl.sda.jdbc.db.user");
        String password = properties.getProperty("pl.sda.jdbc.db.password");
        int port = Integer.parseInt(properties.getProperty("pl.sda.jdbc.db.port"));
        // Krok 7
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setServerName(serverName);
        dataSource.setDatabaseName(databaseName);
        dataSource.setUser(user);
        dataSource.setPassword(password);
        dataSource.setPort(port);
        dataSource.setCharacterEncoding("UTF-8");
        dataSource.setServerTimezone("Europe/Warsaw");
        dataSource.setUseSSL(false);
        dataSource.setAllowPublicKeyRetrieval(true);
        // Krok 8
        return dataSource;
    }
    public Connection getConnection() throws SQLException {
        // Krok 10
        if(dataSource == null){
            throw new IllegalStateException("DataSource is null");
        }
        return dataSource.getConnection();
    }
    public static void main(String[] args) {
        // Krok 1
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // Krok 9
        try(Connection connection = connectionFactory.getConnection()) {
            // Krok 11
            logger.info("Connected database successfully...");
            logger.info("Connection = " + connection);
            logger.info("Database name = " + connection.getCatalog());
            // KONIEC
        } catch (SQLException e) {
            logger.error("Error during using connection", e);
        }
    }
}





