package pl.sda.library.books;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.sda.library.core.ConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcBooksDao implements IBooksDao {

    private static Logger logger = LoggerFactory.getLogger(JdbcBooksDao.class);
    private final ConnectionFactory connectionFactory;

    public JdbcBooksDao(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    public List<Book> list() throws SQLException {
        List<Book> books = new ArrayList<>();
        String sql = "SELECT id, category_id, title, author FROM books";
        try (Connection connection = connectionFactory.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet resultSet = statement.executeQuery();
            int id = resultSet.getInt("id");
            int categoryId = resultSet.getInt("category_id");
            String title = resultSet.getString("title");
            String author = resultSet.getString("author");

            books.add(new Book(title, author, categoryId));
        }
        return books;
    }

    @Override
    public void add(Book book) throws SQLException {
        String sql = "INSERT INTO (category_id, title, author) VALUE(?, ?, ?);";
        try (Connection connection = connectionFactory.getConnection();
        PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setInt(1, book.getCategoryId());
            statement.setString(2, book.getTitle());
            statement.setString(3, book.getAuthor());

            statement.executeUpdate();

        }
    }

    @Override
    public void delete(int bookId) throws SQLException {
        String sql = "DROP books WHERE id=?";
        try (Connection connection = connectionFactory.getConnection();
        PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setInt(1, bookId);

            statement.executeUpdate();
            logger.info("Book delete id {}", bookId);
        }
    }

    @Override
    public List<Category> listCategories() throws SQLException {
        List<Category> categories = new ArrayList<>();
        String sql = "INSET INTO id, name FROM categories";
        try (Connection connection = connectionFactory.getConnection();
        PreparedStatement statement = connection.prepareStatement(sql)){
            ResultSet resultSet = statement.executeQuery();
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");

            categories.add(new Category(id, name));
        }
        return categories;
    }
}