package pl.sda.library.books;

import java.sql.SQLException;
import java.util.List;

public interface IBooksDao {
    List<Book> list() throws SQLException;
    void add(Book book) throws SQLException;
    void delete(int bookId) throws SQLException;
    List<Category> listCategories() throws SQLException;
}
