package pl.sda.zoo_keeper.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.sda.zoo_keeper.dto.AnimalType;
import pl.sda.zoo_keeper.config.ConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class AnimalsTypesDao {
    private static Logger logger = (Logger) LoggerFactory.getLogger(AnimalsTypesDao.class);
    private ConnectionFactory connectionFactory;

    public AnimalsTypesDao(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    /**
     * Method for adding new AnimalType
     *
     * @param animalType type of animal
     * @throws SQLException
     */
    public void add(AnimalType animalType) throws SQLException {
        try (Connection connection = connectionFactory.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "INSERT INTO animals_types(name)" +
                             "VALUES(?)")) {
            statement.setString(1, animalType.getName());
            statement.executeUpdate();

        }
    }

    public List<AnimalType> list() throws SQLException {
        List<AnimalType> animalTypesList = new ArrayList<>();
        try (Connection connection = connectionFactory.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "SELECT id, name FROM animals_types")) {
            ResultSet resultSet = statement.executeQuery(); // wywołanie zapytania
            while (resultSet.next()) {                       // iteracja po elementach listy
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                AnimalType animalType = new AnimalType(id, name); // utworzenie obiektu animalType
                animalTypesList.add(animalType);                    // dodanie do listy
            }

        }
        return animalTypesList;
    }

    public void printId(int id) throws SQLException {
        try (Connection connection = connectionFactory.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "SELECT id, name FROM animals_types")) {
            statement.setInt(1, id);
            statement.executeUpdate();
        }
    }

    public void deleteId(int id) throws SQLException{
        try (Connection connection = connectionFactory.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "DROP FROM animals_types WHERE id=?")) {
            statement.setInt(1, id);
            statement.executeUpdate();
        }
    }

    public void updateId(int id, String name) throws SQLException{
        try (Connection connection = connectionFactory.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "UPDATE animals_types  SET name=?  WHERE id=?")) {
            statement.setString(1, name);
            statement.setInt(2, id);
            statement.executeUpdate();
        }
    }
}
