package pl.sda.zoo_keeper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.sda.zoo_keeper.config.ConnectionFactory;
import pl.sda.zoo_keeper.dao.AnimalsTypesDao;
import pl.sda.zoo_keeper.dto.AnimalType;

import java.sql.SQLException;
import java.util.List;

public class ZooKeeper {
    private static Logger logger = (Logger) LoggerFactory.getLogger(AnimalsTypesDao.class);

    public static void main(String[] args) throws SQLException {
        ConnectionFactory connectionFactory = new ConnectionFactory();

        AnimalsTypesDao animalsTypesDao = new AnimalsTypesDao(connectionFactory);
//        animalsTypesDao.add(new AnimalType(1,"Dog"));
//        animalsTypesDao.add(new AnimalType(2,"Cat"));
//        animalsTypesDao.add(new AnimalType(3,"Horse"));
//        animalsTypesDao.printId(2);
        animalsTypesDao.deleteId(1);
//        animalsTypesDao.updateId(1,"pies");

        List<AnimalType> listOfAnimalTypes = animalsTypesDao.list();
        logger.info("List of types");
        listOfAnimalTypes.forEach(t -> logger.info(t.toString()));
    }
}
