package pl.sda.jdbc.starter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.crypto.Data;
import java.sql.*;
import java.time.LocalDate;

public class CoursesManager {

    private static Logger logger = LoggerFactory.getLogger(CoursesManager.class);
    private ConnectionFactory connectionFactory;

    public CoursesManager(String filename) {
        this.connectionFactory = new ConnectionFactory(filename);
    }

    public void createCoursesTable() throws SQLException {

        try (Connection connection = connectionFactory.getConnection();
             Statement statement = connection.createStatement()) {
            statement.executeUpdate(
                    "CREATE TABLE IF NOT EXISTS courses (" +
                            "id INT NOT NULL AUTO_INCREMENT, " +
                            "name VARCHAR(50)," +
                            "place VARCHAR(50)," +
                            "start_date DATE," +
                            "end_date DATE," +
                            "PRIMARY KEY(id))");
            logger.info("Table courses created.");

//            statement.executeUpdate(
//                    "INSERT INTO courses(name, place, start_date, end_date)" +
//                            "VALUES('JavaGli2', 'Gliwice', '2019-11-15', '2020-09-15')");
//            statement.executeUpdate(
//                    "INSERT INTO courses(name, place, start_date, end_date)" +
//                            "VALUES('JavaGli3', 'Gliwice', '2010-11-15', '2021-09-15')");
//            statement.executeUpdate(
//                    "INSERT INTO courses(name, place, start_date, end_date)" +
//                            "VALUES('JavaGli4', 'Gliwice', '2019-11-15', '2022-09-15')");
//            logger.info("Courses inserted!");

            addCourses("JavaGli4", "Gliwice", LocalDate.of(2022, 02, 15),
                    LocalDate.of(2022, 12, 24));
        }
    }

    public void addCourses(String name, String place, LocalDate startDate, LocalDate endDate) throws SQLException {
        try (Connection connection = connectionFactory.getConnection();
             PreparedStatement statement = connection.prepareStatement("INSERT INTO courses " +
                     "(name, place, start_date, end_date) " +
                     "VALUES (?, ?, ?, ?)")) {

            statement.setString(1, name);
            statement.setString(2, place);
            statement.setDate(3, Date.valueOf(startDate));
            statement.setDate(4, Date.valueOf(endDate));

            statement.executeUpdate();

        }
    }

    public void updateCourses(int id, String name) throws SQLException {
        try (Connection connection = connectionFactory.getConnection();
        PreparedStatement statement = connection.prepareStatement(
                "UPDATE courses SET name=? WHERE id=?")){

            statement.setString(1, name);
            statement.setInt(2, id);

            statement.executeUpdate();

        }
    }

    public void deleteCourses(int id) throws SQLException {
        try (Connection connection = connectionFactory.getConnection();
        PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM courses WHERE id=?")){

            statement.setInt(1, id);
            statement.executeUpdate();

        }
    }


    public void createStudentsTable() throws SQLException {
        try (Connection connection = connectionFactory.getConnection();
             Statement statement = connection.createStatement()) {
            statement.executeUpdate(
                    "CREATE TABLE IF NOT EXISTS students (" +
                            "id INT NOT NULL AUTO_INCREMENT, " +
                            "name VARCHAR(50)," +
                           "courses_id INT," +
                            "description VARCHAR(200)," +
                            "seat VARCHAR(10)," +
                            "PRIMARY KEY(id)," +
                            "FOREIGN KEY (courses_id) REFERENCES courses(id))");
            logger.info("Students courses created.");

//            statement.executeUpdate(
//                    "INSERT INTO students " +
//                            "(name, courses_id, description, seat)" +
//                            "VALUES ('Paweł', '1', 'Nauczenie się podstaw języka i tworzenie aplikacji np. w Kotlinie'," +
//                            " 'A.2.1')");
//            statement.executeUpdate(
//                    "INSERT INTO students " +
//                            "(name, courses_id, description, seat)" +
//                            "VALUES ('Adam', '1', 'Nauczenie się podstaw języka i tworzenie aplikacji np. w Kotlinie', " +
//                            "'A.1.1')");
            addStudent("Adam", 1, "Bo tak", "A.1.1");
            addStudent("Tomasz", 1, " tak chciała żona", "A.2.1");
        }
    }

    public void addStudent(String name, Integer courses_id, String description, String seat) throws SQLException {
        try (Connection connection = connectionFactory.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO students (name, courses_id, " +
                    "description, seat)" +
                    "VALUES (?, ?, ?, ?)");
            statement.setString(1, name);
            statement.setObject(2, courses_id);
            statement.setString(3, description);
            statement.setString(4, seat);

            statement.executeUpdate();
        }
    }

    public void updateStudent(int id, String description, String seat) throws SQLException {
        try (Connection connection = connectionFactory.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "UPDATE students SET description=? seat=? WHERE id=?")) {

            statement.setString(1, description);
            statement.setObject(2, seat);
            statement.setInt(3, id);

            statement.executeUpdate();

        }
    }

    public void createAttendanceListTable() throws SQLException {
        try (Connection connection = connectionFactory.getConnection();
             Statement statement = connection.createStatement()) {
            statement.executeUpdate(
                    "CREATE TABLE IF NOT EXISTS attendance_list (" +
                            "id INT NOT NULL AUTO_INCREMENT, " +
                            "student_id INT," +
                            "courses_id INT," +
                            "date DATETIME," +
                            "PRIMARY KEY (id)," +
                            "FOREIGN KEY (student_id) REFERENCES students(id)," +
                            "FOREIGN KEY (courses_id) REFERENCES courses(id))");
            logger.info("Table: sda_courses.attendance_list created!");

//            statement.executeUpdate("INSERT INTO attendance_list(student_id, course_id, date)" +
//                    " VALUES(1, 1, '2018-06-01')");

            addAttendanceList(1, 1, LocalDate.of(2020, 06, 20));
            logger.info("Table: sda_courses.attendance_list filled with data!");
        }
    }

    public void addAttendanceList(int student_id, int courses_id, LocalDate date) throws SQLException {
        try (Connection connection = connectionFactory.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "INSERT INTO attendance_list (student_id, courses_id, date)" +
                             "VALUES(?, ?, ?)")) {
            statement.setInt(1, student_id);
            statement.setInt(2, courses_id);
            statement.setDate(3, Date.valueOf(date));

            statement.executeUpdate();

        }
    }

    public void dropAllTables() throws SQLException {
        try (Connection connection = connectionFactory.getConnection();
             Statement statement = connection.createStatement()) {
            statement.executeUpdate("DROP TABLE sda_courses.attendance_list;");
            statement.executeUpdate("DROP TABLE sda_courses.students;");
            statement.executeUpdate("DROP TABLE sda_courses.courses;");
            logger.info("All tables dropped");
        }
    }

    public void printAllCourses() throws SQLException {
        try (Connection connection = connectionFactory.getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT id, name, place, start_date, " +
                    "end_date FROM courses");
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String place = resultSet.getString("place");
                Date startDate = resultSet.getDate("start_date");
                Date endDate = resultSet.getDate("end_date");
                logger.info("{}, {}, {}, {} - {}", id, name, place, startDate, endDate);
            }

        }
    }

    public void printAllStudents(int courseId) throws SQLException {
        try (Connection connection = connectionFactory.getConnection();
             PreparedStatement statement = connection.prepareStatement(
                     "SELECT s.id, s.name AS courses_name, s.description, s.seat " +
                             "FROM students AS s" +
                             "LEFT JOIN courses AS C ON s,courses_id = c.id " +
                             "WHERE C.id=?")) {
            statement.setInt(1, courseId);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");
                Date seat = resultSet.getDate("seat");

                logger.info("{}, {}, {}, {}, {}", id, name, description, seat);

            }
        }
    }

    public static void main(String[] args) throws SQLException {

        CoursesManager coursesManager = new CoursesManager("/sda-courses-database.properties");

//        coursesManager.dropAllTables();
//        coursesManager.createCoursesTable();
//        coursesManager.createStudentsTable();
        coursesManager.createAttendanceListTable();
//        coursesManager.printAllCourses();
//        coursesManager.printAllStudents(1);
    }
}
